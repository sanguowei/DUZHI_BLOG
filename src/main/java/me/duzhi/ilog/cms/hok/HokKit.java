package me.duzhi.ilog.cms.hok;

/**
 * @author ashang.peng@aliyun.com
 * @date 二月 14, 2017
 */

public class HokKit {
    /**
     * 注冊Action 插件
     *
     * @param actionClass
     */
    public static void register(Class<? extends Action> actionClass) {
        HokManager.me().registerAction(actionClass);
    }

    /**
     * 解壓Action 插件
     *
     * @param actionClass
     */
    public static void unRegister(Class<? extends Action> actionClass) {
        HokManager.me().unRegisterAction(actionClass);
    }

    /**
     * @param message
     * @param invoke
     * @param action
     * @param <T>
     * @return
     */
    public static  <T> void invoke(HokInvoke.Message message, HokInvoke.Inv<T> invoke, String action) {
         HokManager.me().invoke(message, invoke, action);
    }
}
