/**
 * Copyright (c) 2015-2016, Michael Yang 杨福海 (fuhai999@gmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.jpress.cache;

import com.jfinal.log.Log;
import com.jfinal.plugin.IPlugin;

import io.jpress.cache.impl.J2Cache;
import io.jpress.cache.impl.JEhCache;
import io.jpress.utils.StringUtils;
import me.duzhi.ilog.cms.function.Functions;

public class JCachePlugin implements IPlugin {

    public static final Log log = Log.getLog(JCachePlugin.class);

    public JCachePlugin() {
        Class cl = JEhCache.class;
        String cacheClassName = Functions.Kit.get("ICacheImpl");
        if (StringUtils.isNotBlank(cacheClassName)) {
            log.warn("initial cache , cache name :" + cacheClassName);
            try {
                cl = Class.forName(cacheClassName);
            } catch (ClassNotFoundException e) {
                log.error("initial cache configuration failed ,cache name:" + cacheClassName + "," + e.getMessage(), e);
            }
        }
        CacheManager.me().init(cl);
        log.warn("initial cache success, cache name :" + cacheClassName);
    }

    public JCachePlugin(Class<? extends ICache> clazz) {
        if (clazz == null) {
            throw new RuntimeException("clazz must not be null");
        }
        CacheManager.me().init(clazz);
    }

    public com.jfinal.plugin.activerecord.cache.ICache getCache() {
        return CacheManager.me().getCache();

    }

    @Override
    public boolean start() {
        return true;
    }

    @Override
    public boolean stop() {
        CacheManager.me().destroy();
        return true;
    }

}
